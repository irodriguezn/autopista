package autopista;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author nacho
 */
public class Autopista {

    private LinkedList<Vehiculo> efectivo = new LinkedList<>();
    private LinkedList<Vehiculo> importeExacto = new LinkedList<>();
    private LinkedList<Vehiculo> tarjeta = new LinkedList<>();
    private int numCoches = 0;

    public int getNumCoches() {
        return numCoches;
    }

    public void añadirTarjeta(Vehiculo v) {
        tarjeta.addFirst(v);
        if (v instanceof Coche) {
            numCoches++;
        }

    }

    public void añadirEfectivo(Vehiculo v) {
        efectivo.addFirst(v);
    }

    public void añadirImpExacto(Vehiculo v) {
        importeExacto.addFirst(v);
    }

    public void eliminarTarjeta() {
        tarjeta.removeLast();
    }

    public void eliminarEfectivo() {
        efectivo.removeLast();
    }

    public void eliminarImpExacto() {
        importeExacto.removeLast();
    }

    public String muestraColaTarjeta() {
        String c = "";
        Vehiculo v;
        for (int i = 0; i < tarjeta.size(); i++) {
            v = tarjeta.get(i);
            c += v.toString() + " ";
        }
        return c;
    }

    public String muestraColaEfectvio() {
        String c = "";
        for (Vehiculo v : efectivo) {
            c += v.toString() + " ";
        }
        return c;
    }

    public String muestraColaImpExacto() {
        String c = "";
        Vehiculo v;
        Iterator it = importeExacto.iterator();
        while (it.hasNext()) {
            v = (Vehiculo) it.next();
            c += v.toString() + " ";
        }
        return c;
    }

}
